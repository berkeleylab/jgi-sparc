# SparkReadClust (SpaRC): a scalable tool for read clustering using Apache Spark

SpaRC supports two use cases, local clustering and global clustering. Local clustering exploits read overlap to cluster them (we recommend using it only for long reads), while global clustering further exploits statistics of multiple samples from the same (metagenome) community. We will demonstrate local clustering using a corrected PacBio genome sequencing dataset for identifying potential contaminants (s3://jgi-ga-public/public_datasets/cleaned.fa.gz). We will demonstrate global clustering using a small 10-sample metagenome dataset compiled from [CAMI2](https://openstack.cebitec.uni-bielefeld.de:8080/swift/v1/CAMISIM_MOUSEGUT/)).

## Requirements:

The development environment uses the following settings.

1. ``>=``Spark 2.0.1
2. ``>=``Scala 2.11.8
3. ``>=``Hadoop 2.7.3
4. ``>=``JAVA 1.8

Please edit/use a specific version of build.sbt according to your version of Spark and related software.    

## Prerequisites

SpaRC can be run from a single machine, or from a managed cloud environment. Here we demonstrate how to run it on AWS EMR (assuming aws-cli installed and configured). 

Launching SpaRC on AWS EMR requires a script (aws-launch-sparc.bash) from the repo, so first we need to clone [SpaRC](git clone https://bitbucket.org/berkeleylab/jgi-sparc.git/) into your local computer 

```shell
$ git clone https://bitbucket.org/berkeleylab/jgi-sparc.git/
$ cd ./jgi-sparc
$ ./scripts_run_sparc/aws-launch-sparc.bash --help
<Launch an EMR cluster on AWS --help>
        --key           Your private EC2 key name (without the .pem), required
        --notebook_dir  The S3 path to mount on the master node, required.
        --logs          The S3 path to keep EMR bootstrap, required.
        --master        EMR master node instance type. Default: r5.xlarge
        --node          EMR slave node instance type. Default: r4.2xlarge
        --size          The number of slave nodes. Default: 2
        --volume        Disk capacity(GB) for every node. Default: 100
        --price         The max price you\'d like to provide for slave node on spot node. Default: 0.6
        --release       EMR version. Default: emr-5.17.0
        --jupyter       Whether or not you want to use Jupyter environment<shell|notebook> to run SpaRC. true or false, default: true
Example: ./scripts_run_sparc/aws-launch-sparc.bash --master r5.xlarge --node r4.2xlarge --size 2 --volume 100  --price 0.6 --release emr-5.17.0 --key KEYNAME --jupyter true  --logs s3://SPARCLOGS --notebook_dir s3://SPARCNOTEBOOKS
```

The above example launches a cluster and if successful, print out the cluster-id. Wait a few minutes until the cluster is up, then get the master node IP using "aws emr list-instances --cluster-id XXXX". Now let's hop on the master node and run SpaRC!

```shell
$ ssh -i "EC2 key with .pem" -L 8888:localhost:8888 hadoop@XXXX (master node IP)
$ If you have jupyter enabled, it is available at localhost:8888 (in client browser)
```

## Preparing INPUT

### Use case #1, local clustering on a PacBio genome sequencing dataset. 

s3://SPARCNOTEBOOKS is mounted to /mnt/SPARCNOTEBOOKS on the master node. Now let's copy the raw input data <.fasta|.fastq|.seq> there. 

```shell
$ aws s3 cp s3://jgi-ga-public/public_datasets/cleaned.fa.gz /mnt/SPARCNOTEBOOKS
$ gzip -d /mnt/SPARCNOTEBOOKS/cleaned.fa.gz 
```

For .fa.gz with sequences spanning muiltple lines, re-format them into single lines:

```shell
$ awk '/^>/&&NR>1{print "";}{ printf "%s",/^>/ ? $0"\t":$0 }'  /mnt/SPARCNOTEBOOKS/cleaned.fa > /mnt/SPARCNOTEBOOKS/cleaned1line.fa
```

### Use case #2. Global clusterng with 10 metagenome samples

```shell
$ aws s3 cp s3://jgi-ga-public/public_datasets/CAMI210samples /mnt/SPARCNOTEBOOKS/ 
```

## Run SpaRC in batch mode

Edit the config_local.txt file before running the following pipeline:

```shell
$ cd /mnt
$ git clone https://bitbucket.org/berkeleylab/jgi-sparc.git/
$ cd ./jgi-sparc
$ ./scripts_run_sparc/run-sparc.sh --mode cluster --input /mnt/SPARCNOTEBOOKS/INPUTFILE --config config_local.txt --module All
```

**Note:**

1. **--mode:** Deployment mode<single|cluster> is required, for AWS EMR set it to cluster mode.
2. **--input:**  Input file path in master node is required, SpaRC supports <.fasta|.fastq|.seq>.
3. **--config** We provide two examples for CONFIG.txt: config_local.txt and config_global.txt.
3. **--module** Module should be run <All|Preprocess|KmerMapReads|MinimizerMapReads|GraphGen|GraphLPA|CCAddSeq>,All means run all modules in turn, Required.

## Logs and results

Same as the input path there will be 2 files: logs and result.

**logs** important logs are prefixed with "AAAA":

```shell
    $  grep  "AAAA" ....
    the result is like below:
    $ AAAA number of reads: 
    $ AAAA number of cluster:
    $ AAAA ......
```

**result** is a tsv text file: readsID \t clusterID

## Terminate SpaRC EMR cluster

If everything goes well, the cluster can be terminated, all files should have been automatically saved to s3://SPARCNOTEBOOKS/result

# License

SpaRC  Copyright (c) 2018, The Regents of the University of California, through Lawrence Berkeley National Laboratory (subject to receipt of any required approvals from the U.S. Dept. of Energy).  All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

 (1) Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 (2) Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 (3) Neither the name of the University of California, Lawrence Berkeley National Laboratory, U.S. Dept. of Energy nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

You are under no obligation whatsoever to provide any bug fixes, patches, or upgrades to the features, functionality or performance of the source code ("Enhancements") to anyone; however, if you choose to make your Enhancements available either publicly, or directly to Lawrence Berkeley National Laboratory, without imposing a separate written license agreement for such Enhancements, then you hereby grant the following license: a  non-exclusive, royalty-free perpetual license to install, use, modify, prepare derivative works, incorporate into other computer software, distribute, and sublicense such enhancements or derivative works thereof, in binary and source code form.