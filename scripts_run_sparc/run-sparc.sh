#!/usr/bin/env bash

SpaRChelp ()
{
echo "<Run SpaRC --help>
        --mode          Deploy Spark on <single|cluster>, required
        --input         The raw input file, the format should be one of <fasta|fastq|seq>, required.
        --config        Parameter file for <config_local.txt|config_global.txt>, required.
        --module        Which module should be run <All|Preprocess|KmerMapReads|MinimizerMapReads|GraphGen|GraphLPA|CCAddSeq>,All means run all modules in turn, Required.
Example: $0 --mode cluster --input input_data.seq --config config_local.txt --module all"
exit 0
}

if [ $# -lt 1 ]; then
        SpaRChelp
fi
# get input parameters
while [ $# -gt 0 ]; do
    case "$1" in
    --mode)
      shift
      MODE=$1
      ;;
    --input)
      shift
      INPUT=$1
      ;;
    --config)
      shift
      CONFIG=$1
      ;;
    --module)
      shift
      MODULE=$1
      ;;
    --help)
      shift
      SpaRChelp
      ;;
    -*)
      echo "Unknown options: $1"
      SpaRChelp
      ;;
    *)
      break;
      ;;
    esac
    shift
done

##locate the dir
SCRIPTPATH="$( cd "$(dirname "$0")" ;cd ..; pwd)"
DIR="$(dirname  "$INPUT")"
PATH=$SCRIPTPATH:$PATH
PATH=$DIR:$PATH
if (test -f $INPUT) ;then
   INPUT_DATA=`basename $INPUT`  #single file
else
   datadir="$(cd $INPUT/..;ls `basename $INPUT`*)"
   for state in $datadir
        do INPUT_DATA+=`basename $INPUT`/$state"," #single
done
   INPUT_DATA=${INPUT_DATA::-1}
fi
echo $INPUT_DATA
cd $DIR;
if !(test -d logs) ;then
    mkdir logs
fi
if !(test -d result); then
    mkdir result
fi
LOGS=$DIR/logs
echo $LOGS
RESULT=$DIR/result
cd $SCRIPTPATH; . $SCRIPTPATH/scripts_run_sparc/$CONFIG

single(){
##spark env configure:
    echo "loading the single configure"
    SPARK_SUBMIT=$SPARK_HOME/bin/spark-submit
    MASTER=local[$executor_cores]
    INPUT_DATA=$INPUT
}
cluster(){
    echo "loading the cluster configure"
    SPARK_SUBMIT=spark-submit
    MASTER=yarn
    hadoop fs -D dfs.blocksize=33554432 -put $INPUT
    hadoop fs -mkdir result
    RESULT="result"
    #hadoop fa -put ...
}
if [ "$MODE" = "single" ]; then
        single
        . $SCRIPTPATH/scripts_run_sparc/sparc.sh
elif [ "$MODE" = "cluster" ];then
       cluster
       . $SCRIPTPATH/scripts_run_sparc/sparc.sh
        hadoop fs -getmerge $RESULT/ccaddseq_output `dirname $INPUT`/result/ccaddseq_output
fi