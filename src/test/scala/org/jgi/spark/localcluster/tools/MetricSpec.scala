package org.jgi.spark.localcluster.tools

import com.holdenkarau.spark.testing.SharedSparkContext
import org.scalatest.{FlatSpec, Matchers, _}
import sext._


class MetricSpec extends FlatSpec with Matchers with BeforeAndAfter with SharedSparkContext {

  "parse command line" should "be good" in {
    val cfg = Metric.parse_command_line("-l  data -k data  -o tmp ".split(" ")).get
    cfg.lpa_file should be("data")
    cfg.key_file should be("data")
  }

  "Metric" should "work on the global files" in {
    val cfg = Metric.parse_command_line(
      "-l data/ccaddseq_output  -k data/label100 --flag CAMI -o tmp/metric_out -n 2 ".split(" ")
        .filter(_.nonEmpty)).get
    println(s"called with arguments\n${cfg.valueTreeString}")

    Metric.run(cfg, sc)
    //Thread.sleep(1000 * 10000)
  }

  "Metric" should "work on the local files" in {
    val cfg = Metric.parse_command_line(
      "-l data/local/ccaddseq_output  -k data/mock-key.txt --flag MOCK -o tmp/metric_out -n 2 ".split(" ")
        .filter(_.nonEmpty)).get
    println(s"called with arguments\n${cfg.valueTreeString}")

    Metric.run(cfg, sc)
    //Thread.sleep(1000 * 10000)
  }

  "Metric" should "work on the files" in {
    val cfg = Metric.parse_command_line(
      "-l data/lpa1 -k data/key1 --flag CAMI -o tmp/metric_out -n 2 ".split(" ")
        .filter(_.nonEmpty)).get
    println(s"called with arguments\n${cfg.valueTreeString}")

    Metric.run(cfg, sc)
    //Thread.sleep(1000 * 10000)
  }

}
