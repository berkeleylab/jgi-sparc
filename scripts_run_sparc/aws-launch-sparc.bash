#!/usr/bin/env bash
# spawn a small cluster to run spark applications
# by Zhong Wang @ lbl.gov

# some defaults
MASTER="r5.xlarge"
NODE="r4.2xlarge"
SIZE=2
VOL=100
PRICE=0.6
RELEASE="emr-5.17.0"
JUPYTER=true

# usage message
usage ()
{
echo "<Launch an EMR cluster on AWS --help>
        --key           Your private EC2 key name (without the .pem), required
        --notebook_dir  The S3 path to mount on the master node, required.
        --logs          The S3 path to keep EMR bootstrap, required.
        --master        EMR master node instance type. Default: r5.xlarge
        --node          EMR slave node instance type. Default: r4.2xlarge
        --size          The number of slave nodes. Default: 2
        --volume        Disk capacity(GB) for every node. Default: 100
        --price         The max price you\'d like to provide for slave node on spot node. Default: 0.6
        --release       EMR version. Default: emr-5.17.0
        --jupyter       Whether or not you want to use Jupyter environment<shell|notebook> to run SpaRC. true or false, default: true
Example: $0 --master r5.xlarge --node r4.2xlarge --size 2 --volume 100  --price 0.6 --release emr-5.17.0 \
--key KEYNAME --jupyter true  --logs s3://SPARCLOGS --notebook_dir s3://SPARCNOTEBOOKS "
exit 0
}
if [ $# -lt 3 ]; then
	usage
fi

# get input parameters
while [ $# -gt 0 ]; do
    case "$1" in
    --master)
      shift
      MASTER=$1
      ;;
    --node)
      shift
      NODE=$1
      ;;
    --size)
      shift
      SIZE=$1
      ;;
    --volume)
      shift
      VOL=$1
      ;;
    --price)
      shift
      PRICE=$1
      ;;
    --release)
      shift
      RELEASE=$1
      ;;
    --jupyter)
      shift
      JUPYTER=$1
      if [ "$JUPYTER" = true ]; then
      echo "Launch a cluster with Jupyter Notebook"
      BOOTSTRAP="s3://kexue-notebook/artifacts-new/emr-boostrap-notebook.sh"
      else
      echo "Launch a cluster with shell"
      BOOTSTRAP="s3://kexue-notebook/artifacts-new/emr-boostrap-shell.sh"
      fi
      ;;
    --key)
      shift
      KEY=$1
      ;;
    --notebook_dir)
      shift
      NOTEBOOK_DIR=$1
      ;;
    --logs)
      shift
      LOGS=$1
      ;;
    --help)
      shift
      usage
      ;;
    -*)
      echo "Unknown options: $1"
      usage
      ;;
    *)
      break;
      ;;
    esac
    shift
done

aws emr create-cluster --release-label ${RELEASE} \
  --name "Run-SpaRC-notebook" --no-visible-to-all-users \
  --applications Name=Hadoop Name=Spark Name=Ganglia \
  --ec2-attributes KeyName=${KEY},SubnetId=subnet-01a65bbdd29523d1a,InstanceProfile=EMR_EC2_DefaultRole \
  --service-role EMR_DefaultRole \
  --instance-groups "InstanceGroupType=MASTER,InstanceCount=1,InstanceType=${MASTER},\
EbsConfiguration={EbsOptimized=true,EbsBlockDeviceConfigs=[{VolumeSpecification={VolumeType=gp2,SizeInGB=${VOL}}}]}" \
  "InstanceGroupType=CORE,InstanceCount=${SIZE},InstanceType=${NODE},BidPrice=${PRICE}" \
  --region us-east-1 \
  --log-uri ${LOGS} \
  --bootstrap-actions Name="SpaRC-jupyter",Path=$BOOTSTRAP,Args=[--notebook-dir,$NOTEBOOK_DIR]